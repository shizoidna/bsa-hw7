import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const Header = ({ data }) => (
  <div className={styles.container}>
    <div className={styles.infoGroupLeft}>
      <h4>{data.chatName}</h4>
      <span> {data.amountOfUsers} participants </span>
      <span> {data.amountOfMessages} messages</span>
    </div>
    <div className={styles.infoGroupRight}>
      <span> last message at {data.lastMessageDate}</span>
    </div>
  </div>
);

Header.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Header;
