import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

import moment from 'moment';
import styles from './styles.module.css';

const Message = ({ message, myId, isLiked }) => {
  const isMyMessage = message.userId === myId;
  return (
    <div className={styles.container}>
      <div>
        {isMyMessage && (
          <img src={message.avatar} alt="avatar" className={styles.avatar} />
        )}
      </div>
      <div className={styles.textGroup}>
        <p className={styles.message}> {message.text}, {isLiked} </p>
        <span className={styles.date}>Wrote at {moment(message.createdAt).format('lll')}</span>
        {/* eslint-disable-next-line max-len */}
        <Label basic size="small" as="a" className={styles.toolbarBtn}>
          HELLOOO
          <Icon name="american sign language interpreting" />
          {isLiked}
        </Label>
      </div>
    </div>
  );
};

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
  myId: PropTypes.string.isRequired,
  isLiked: PropTypes.bool.isRequired,
};

export default Message;

