import React from 'react';
import logo from './logo.svg';
import Header from './components/Header';
import Message from './components/Message';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Logo
          </p>
        </header>
        <div className="ChatApp">
          <Header data={{
            chatName: 'MyChat',
            amountOfUsers: 45,
            amountOfMessages: 563,
            lastMessageDate: '12.08.2020 01:30am',
          }}
          />
          <Message
            message={{
              id: '80f08600-1b8f-11e8-9629-c7eca82aa7bd',
              text: "I don’t *** understand. It's the Panama accounts",
              user: 'Ruth',
              // eslint-disable-next-line max-len
              avatar: 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
              userId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
              editedAt: '',
              createdAt: '2020-07-16T19:48:12.936Z',
            }}
            myId="9e243930-83c9-11e9-8e0c-8f1a686f4ce4"
            // reactMessage={(id, isLiked) => console.log('REACT MESSAGE', id, isLiked)}
            isLiked
          />
        </div>
        <footer className="App-footer">
          <span className="copyright">
            &copy; Copyright
          </span>
        </footer>
      </div>
    );
  }
}

export default App;
